import os

VERSION = '1.3.3'

# TODO Remove env.variables that are not used in import-to-elastic

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")

HTTPS_PROXY = os.getenv("https_proxy")

ES_TIMEOUT = int(os.getenv('ES_TIMEOUT', "30"))
BATCH_SIZE = int(os.getenv('PG_BATCH_SIZE', 10000))

ES_JOBLINKS_ALIAS = os.getenv('ES_JOBLINKS_ALIAS', 'joblinks')
JOBLINKS_STDIN_NAME = os.getenv('JOBLINKS_STDIN_NAME', None)  # optional, added to index name at import from stdin

JOBLINKS_FILE_PATTERN = os.getenv('JOBLINKS_FILE_PATTERN', 'output_202*.json')
JOBLINKS_FOLDER = os.getenv('JOBLINKS_FOLDER', 'resources')
JOBLINKS_READ_FROM_FILE = os.getenv('JOBLINKS_READ_FROM_FILE', 'false').lower() == 'true'


# don't switch alias to new index if number of ads in new index < current index * coefficient
NEW_ADS_COEF = float(os.getenv('NEW_ADS_COEF', 0.8))

# settings for loguru
LOG_LEVEL= os.getenv("LOG_LEVEL", "INFO")
