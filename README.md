# import-to-elastic

The last part (i.e. Processor) of the Joblinks Pipeline.

## Description

Imports joblinks ads into an ElasticSearch index. The ads should be sent in from stdin, but for keeping development and
test simple there is support for reading ads from file too 

## Setup for developers and running as a Docker image

see the file `docs/developer_setup.md`
