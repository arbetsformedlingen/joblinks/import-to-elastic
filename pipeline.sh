#!/usr/bin/env bash
set -eEu -o pipefail

namespace="pipeline-test" # default to a dev env
secrets=""
ads_coef=""
envlist_args=""
while [ "$#" -gt 0 ]; do
    case "$1" in
        --env)       envlist_args="$envlist_args $2"; shift;;
        --namespace) namespace="$2"; shift ;;
        --coef)      ads_coef="$2"; shift;;
        --secrets) secrets="$2"; shift;;
    *)       echo "**** unknown opt: $1" >&2; exit 1;;
    esac
    shift
done

if [ -z "$namespace" ]; then echo "**** supply namespace" >&2; exit 1; fi
if [ -z "$secrets" ];   then echo "**** supply secrets" >&2; exit 1; fi

# Source secret ES_PWD and ES_HOST
. "$secrets" || exit 1

if [ -z "$ES_HOST" ]; then
    echo "**** ES_HOST must be set" >&2; exit 1
fi

envlist="$envlist_args ES_HOST=$ES_HOST"

if [ -z "$ES_PWD" ]; then
    echo "**** WARNING: ES_PWD is not set. Running locally?" >&2;
else
    envlist="$envlist ES_PWD=$ES_PWD"
fi

envlist="$envlist ES_JOBLINKS_ALIAS=joblinks-$namespace"


if [ -n "$ads_coef" ]; then
    envlist="$envlist NEW_ADS_COEF=$ads_coef"
fi


# Run the application
echo "info: starting app with cmdline: env $envlist python3 ./main.py" >&2
env $envlist python3 ./main.py
