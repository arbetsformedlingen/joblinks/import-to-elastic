import pytest

from repository import converter
from tests.unit_tests.test_resources import ads


@pytest.mark.integration
def test_convert_ad():
    result = converter.convert_ad(ads.ad_before_conversion)
    assert result == ads.ad_after_conversion


@pytest.mark.parametrize("element, key, replacement_val, expected ", [
    ({'municipality_concept_id': '', 'municipality': '', 'region_concept_id': '', 'region': '',
      'country_concept_id': 'i46j_HmG_v64', 'country': 'Sverige'}, 'city', '', ''),
    ({'municipality_concept_id': '', 'municipality': '', 'region_concept_id': '', 'region': '',
      'country_concept_id': 'i46j_HmG_v64', 'country': 'Sverige'}, 'country', '', 'Sverige')
])
@pytest.mark.unit
def test_null_safe_value(element, key, replacement_val, expected):
    assert expected == converter.get_null_safe_value(element, key, replacement_val)
