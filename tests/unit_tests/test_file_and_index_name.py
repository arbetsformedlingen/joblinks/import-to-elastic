from datetime import datetime
import pytest

from repository.in_data_handling import get_timestamp_from_filename, get_filename_by_pattern, get_path_and_pattern
from repository.elastic import format_index_name
from repository.path_util import root_dir
import settings


@pytest.mark.unit
@pytest.mark.parametrize('input_path, expected', [
    ('C:\\path\\to\\some\\repository\\../resources\\output_2020-11-27-05-39-54.json', 'file_2020-11-27-05-39-54'),
    ('output_2020-11-27-05-39-54.json', 'file_2020-11-27-05-39-54'),
    ('/resources/output_2020-11-27-05-39-54.json', 'file_2020-11-27-05-39-54'),
    ('//resources//output_2020-11-27-05-39-54.json', 'file_2020-11-27-05-39-54'),
    ('output_2020-11-27-05-39-54', 'file_2020-11-27-05-39-54'),
    ('output_2020-11-27-05-39-54.json', 'file_2020-11-27-05-39-54'),
    ('output_2020-11-27-05.39.54', 'file_2020-11-27-05.39.54'),
    ('output_2020-11-27-05.39.54.json', 'file_2020-11-27-05.39.54'),
])
def test_get_timestamp_from_filename(input_path, expected):
    result = get_timestamp_from_filename(input_path)
    assert result == expected


@pytest.mark.unit
def test_index_name_builder():
    timestamp_from_file = '2020-11-27-05-39-54'
    now = datetime.now().strftime('%Y%m%d-%H.%M')
    es_alias = settings.ES_JOBLINKS_ALIAS
    expected = f"{es_alias}-{timestamp_from_file}-{now}"

    result = format_index_name(es_alias, timestamp_from_file, now)
    assert result == expected


@pytest.mark.unit
def test_filename_by_pattern():
    dir_for_import_files = root_dir / 'tests' / 'unit_tests' / 'resources'
    path_and_pattern = get_path_and_pattern(dir_for_import_files)
    result = get_filename_by_pattern(path_and_pattern)
    assert 'test_file.json' in result


@pytest.mark.unit
def test_filename_by_pattern_error_handling():
    pattern = 'non-existent-path'
    try:
        r = get_filename_by_pattern(pattern)
    except SystemExit as e:
        pass
    else:
        pytest.fail(f"expected an error but got {r} ")


@pytest.mark.unit
def test_path():
    result = get_path_and_pattern()
    assert settings.JOBLINKS_FILE_PATTERN in result
