Changelog Application import-to-elastic
===============================
# 1.3.3
* Changes Docker image

# 1.3.2
* Add ES_TIMEOUT environment variable for Elastic operations and fix bug in error handling

# 1.3.1
* Update Python version in Dockerfile

# 1.3.0
* Package installation now uses poetry.
* Added so application can be run on localhost with local secrets file. No password required.
* Update Python version in Dockerfile to 3.10.7
* Update dependencies
* Remove "jobtech-common" since it's not needed anymore

# 1.2.0
* Changed logger from logger to loguru

# 1.1.1
* Install 'jobtech-common' and 'valuestore' from GitLab with commit hash

# 1.1.0
* added some date handling for redundancy, new field: date_to_display_as_publish_date

# 1.0.2
* removed date handling logic and use date from ad

# 1.0.1
* add check for number of coming ads and in current index
* use datePosted date from ad if available, use firstSeen date otherwise

# 1.0.0
* initial release
