from zeep import Client
from zeep.transports import Transport
from zeep.helpers import serialize_object
from taxonomy import taxonomy_settings

transport = Transport(timeout=2000, operation_timeout=5000)
wsdl = taxonomy_settings.TAXONOMY_SERVICE_URL + "?WSDL"
client = Client(wsdl=wsdl, transport=transport)
# This step is necessary to handle incorrectly
# specified service url in the open api.
service = client.create_service("{urn:ams.se:Taxonomy}TaxonomiServiceSoap12",
                                taxonomy_settings.TAXONOMY_SERVICE_URL)


def get_taxonomy_version():
    result = service.GetVersionInformations()
    return serialize_object(result)
